Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: libstrangle
Upstream-Contact: Björn Spindel <bjorn.spindel@protonmail.com>
Source: https://gitlab.com/torkel104/libstrangle
Files-Excluded: screenshots/picmip_quake.png
Comment: The screenshot has been removed since it lacked copyright information.

Files: *
Copyright: 2016-2020 Björn Spindel
License:   GPL-3.0+
 This file is part of libstrangle.
 .
 libstrangle is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 .
 libstrangle is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with libstrangle.  If not, see <http://www.gnu.org/licenses/>.
 .
 On Debian systems, the complete text of the GNU General Public License
 Version 3 can be found in `/usr/share/common-licenses/GPL-3'.

Files:     include/vulkan/*
Copyright: 2014-2020 The Khronos Group Inc.
           2015 The Android Open Source Project
           2015-2017 LunarG, Inc.
           2015-2017 Valve Corporation
License:   Apache-2.0
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
 .
 http://www.apache.org/licenses/LICENSE-2.0
 .
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
 .
 This header is generated from the Khronos Vulkan XML API Registry.
 .
 On Debian systems, the complete text of the Apache License Version 2.0
  can be found in `/usr/share/common-licenses/Apache-2.0'.

Files:     include/mesa/util/macros.h
           include/vulkan/vk_util.h
           src/vulkan/overlay.cpp
           include/mesa/c99_compat.h
Copyright: 2014-2019 Intel Corporation
           2020 Björn Spindel
           2007-2013 VMware, Inc.
License:   MIT
 Permission is hereby granted, free of charge, to any person obtaining a
 copy of this software and associated documentation files (the "Software"),
 to deal in the Software without restriction, including without limitation
 the rights to use, copy, modify, merge, publish, distribute, sublicense,
 and/or sell copies of the Software, and to permit persons to whom the
 Software is furnished to do so, subject to the following conditions:
 .
 The above copyright notice and this permission notice (including the next
 paragraph) shall be included in all copies or substantial portions of the
 Software.
 .
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
 THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 IN THE SOFTWARE.

Files:     src/vulkan/vk_enum_to_str.c
           src/vulkan/vk_enum_to_str.h
           include/mesa/no_extern_c.h
           include/mesa/c11_compat.h
Copyright: 2017, 2019 Intel Corporation
           2014 VMware, Inc.
License:   Expat
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 .
 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.
 .
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.

Files:     include/vulkan/vulkan.hpp
Copyright: 2015-2020 The Khronos Group Inc.
License:   Apache-2.0-with-exceptions
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
 .
     http://www.apache.org/licenses/LICENSE-2.0
 .
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
 .
 ---- Exceptions to the Apache 2.0 License: ----
 .
 As an exception, if you use this Software to generate code and portions of
 this Software are embedded into the generated code as a result, you may
 redistribute such product without providing attribution as would otherwise
 be required by Sections 4(a), 4(b) and 4(d) of the License.
 .
 In addition, if you combine or link code generated by this Software with
 software that is licensed under the GPLv2 or the LGPL v2.0 or 2.1
 ("`Combined Software`") and if a court of competent jurisdiction determines
 that the patent provision (Section 3), the indemnity provision (Section 9)
 or other Section of the License conflicts with the conditions of the
 applicable GPL or LGPL license, you may retroactively and prospectively
 choose to deem waived or otherwise exclude such Section(s) of the License,
 but only in their entirety and only with respect to the Combined Software.
 .
 On Debian systems, the complete text of the Apache License Version 2.0
  can be found in `/usr/share/common-licenses/Apache-2.0'.
